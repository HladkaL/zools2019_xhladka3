//
// Created by hladk on 30.04.2019.
//

#include "Surovina.h"

Surovina::Surovina(string nazev, int pocet, int id) {
    m_nazev = nazev;
    m_pocet = pocet;
    m_id = id;
}

void Surovina::prictiPocet(int kolik) {
    m_pocet = m_pocet + kolik;
}

bool Surovina::odectiPocet(int kolik) {
    if (m_pocet >= kolik) {
        m_pocet = m_pocet - kolik;
        return true;
    } else {
        cout << "Mas malo surovin!" << endl;
        return false;
    }
}

string Surovina::getNazev() {
    return m_nazev;
}

int Surovina::getPocet() {
    return m_pocet;
}
