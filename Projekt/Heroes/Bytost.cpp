//
// Created by hladk on 15.04.2019.
//

#include "Bytost.h"

Bytost::Bytost(string nazev, int sila, int pocet, int cena, int poziceX, int poziceY, string kod) : HerniObjekt(poziceX,
                                                                                                                poziceY,
                                                                                                                kod) {
    m_nazev = nazev;
    m_sila = sila;
    m_cena = cena;
    setPocet(pocet);

}

int Bytost::getSila() {
    return m_sila;
}

int Bytost::getCelkemBytosti() {
    return m_pocet;
}

string Bytost::getNazev() {
    return m_nazev;
}

Bytost *Bytost::createBytost(int typ, int kolik) {
    switch (typ) {
        case 1: {

            Bytost *pesak = new Bytost("Rytíř", 2, kolik, 20, 0, 0, "R0");
            return pesak;
        }
        case 2: {
            return new Bytost("Střelec", 3, kolik, 30, 0, 0, "S0");
        }
    }
}

void Bytost::setPocet(int kolik) {
    if (kolik >= 0) {
        m_pocet = kolik;
    } else {
        m_pocet = 0;
    }
}

int Bytost::getCelkovaSila() {
    return (m_sila * m_pocet);
}



