//
// Created by hladk on 28.04.2019.
//

#ifndef HEROES_HRA_H
#define HEROES_HRA_H

#include <iostream>
#include "Mapa.h"
#include "HerniObjekt.h"
#include "Hrad.h"
#include "Hrdina.h"
#include "Bytost.h"


class Hra {
private:
    Mapa *m_mapa;


public:
    void startHry();
    void tahHry();
    void KonecHry();

};


#endif //HEROES_HRA_H
