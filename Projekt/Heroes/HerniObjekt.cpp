//
// Created by hladk on 15.04.2019.
//

#include "HerniObjekt.h"

HerniObjekt::HerniObjekt(int poziceX, int poziceY, std::string kod) {
    m_poziceX = poziceX;
    m_poziceY = poziceY;
    m_kod = kod;
}

std::string HerniObjekt::getKod() {
    return m_kod;
}

int HerniObjekt::getPoziceX() {
    return m_poziceX;
}

int HerniObjekt::getPoziceY() {
    return m_poziceY;
}

void HerniObjekt::setPoziceX(int x) {
    m_poziceX = x;
}

void HerniObjekt::setPoziceY(int y) {
    m_poziceY = y;
}

