//
// Created by hladk on 30.04.2019.
//

#ifndef HEROES_SUROVINA_H
#define HEROES_SUROVINA_H
#include <iostream>
using namespace std;

class Surovina {
string m_nazev;
int m_pocet;
int m_id;
public:
    Surovina(string nazev, int pocet, int id);
    void prictiPocet(int kolik);
    bool odectiPocet(int kolik);
    string getNazev();
    int getPocet();

};


#endif //HEROES_SUROVINA_H
