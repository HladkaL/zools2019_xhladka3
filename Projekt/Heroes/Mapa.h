//
// Created by hladk on 20.04.2019.
//

#ifndef HEROES_MAPA_H
#define HEROES_MAPA_H
#include <iostream>
#include <vector>
#include "HerniObjekt.h"
#include "Bytost.h"
#include "Hrad.h"
#include "Hrdina.h"

using namespace std;

class Mapa {
private:
    vector<vector<HerniObjekt*>> m_mapa;
    Hrdina* hrdinaNaTahu;
    Hrdina* hrdinaCekajici;

    void umistiAVytvorHrady();
    void umistiAVytvorHrdiny();
    int podminkaPohybuHrdinou(Hrdina* hrdina, int x, int y);
    void posunHrdinu(Hrdina *hrdina, int x, int y);



public:
    Hrdina* getHrdinaNaTahu();
    Hrdina* getHrdinaCekajici();
    Mapa(int sirka, int vyska);
    void vynulovat();
    void printMapa();
    void inicializaceHernihoPole();
    HerniObjekt* getHerniObjekt(int x, int y);
    void umistiHerniObjekt(int sirka, int delka, HerniObjekt* herniObjekt);
    void pohniHrdinou(Hrdina* hrdina);


    void boj(Hrdina * kdo, HerniObjekt * skym);
    ~Mapa();

    void setHrdinaCekajici(Hrdina* hrdina);

    void setHrdinaNaTahu(Hrdina *pHrdina);


};




#endif //HEROES_MAPA_H
