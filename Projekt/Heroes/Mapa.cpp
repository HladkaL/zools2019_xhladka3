//
// Created by hladk on 20.04.2019.
//

#include "Mapa.h"

Mapa::Mapa(int sirka, int vyska) {
    for (int i = 0; i < sirka; i++) {
        vector<HerniObjekt *> vectorHernichObjektu;

        for (int j = 0; j < vyska; j++) {
            vectorHernichObjektu.push_back(nullptr);
        }

        m_mapa.push_back(vectorHernichObjektu);
    }
}

void Mapa::vynulovat() {
    for (int i = 0; i < m_mapa.size(); i++) {
        for (int j = 0; j < m_mapa.at(i).size(); j++) {
            m_mapa.at(i).at(j) = nullptr;
        }
    }
}

void Mapa::printMapa() {
    cout << "__________________________________________________________________" << endl;
    cout << "Vykresleni mapy: " << endl;
    cout << "      ";
    for (int i = 0; i < m_mapa.size(); i++) {
        cout << "  " << i << "  |";
    }
    cout << endl;
    for (int i = 0; i < m_mapa.size(); i++) {
        cout << "  " << i << "  |";
        for (int j = 0; j < m_mapa.at(i).size(); j++) {
            if (m_mapa.at(i).at(j) == nullptr) {
                cout << "  ??  ";
            } else {

                cout << "  " << m_mapa.at(i).at(j)->getKod() << "  ";
            }
        }
        cout << endl;

    }
    cout << "P...pevnost;  H...hrdina; R...rytir; S...strelec." << endl;
    cout << "0...bez majitele; 1...hrac1; 2...hrac2" << endl;
}

void Mapa::umistiHerniObjekt(int sirka, int delka, HerniObjekt *herniObjekt) {
    herniObjekt->setPoziceX(sirka); herniObjekt->setPoziceY(delka);
    m_mapa.at(delka).at(sirka) = herniObjekt;
}

void Mapa::umistiAVytvorHrady() {
    cout << "__________________________________________________________________" << endl;
    cout << "První hráč: " << endl;
    int i = 0;
    Hrad *hrad1 = new Hrad(i, i, "P1");
    umistiHerniObjekt(i, i, hrad1);

    cout << "__________________________________________________________________" << endl;
    cout << "Druhý hráč:" << endl;
    int m = 9;
    //int m = m_mapa.size();   // umisteni hradu do posledniho policka matice mxm
    Hrad *hrad2 = new Hrad(m, m, "P2");
    umistiHerniObjekt(m, m, hrad2);
}

void Mapa::inicializaceHernihoPole() {
    umistiAVytvorHrdiny();
    umistiAVytvorHrady();
    umistiHerniObjekt(1,1,Bytost::createBytost(1,5));
    umistiHerniObjekt(3,2,Bytost::createBytost(1,5));
    umistiHerniObjekt(3,5,Bytost::createBytost(2,3));
    umistiHerniObjekt(6,7,Bytost::createBytost(2,2));
    umistiHerniObjekt(8,8,Bytost::createBytost(2,1));
    umistiHerniObjekt(8,1,Bytost::createBytost(1,8));
    umistiHerniObjekt(2,7,Bytost::createBytost(1,5));

}

Mapa::~Mapa() {
    for (int i = 0; i < m_mapa.size(); i++) {
        for (int j = 0; j < m_mapa.at(i).size(); j++) {
            delete (m_mapa.at(i).at(j));
        }
    }
}

void Mapa::umistiAVytvorHrdiny() {
    cout << "__________________________________________________________________" << endl;
    cout << "První hráč: " << endl;
    hrdinaNaTahu= new Hrdina(0, 1, "H1");
    umistiHerniObjekt(0, 1, hrdinaNaTahu);

    cout << "__________________________________________________________________" << endl;
    cout << "Druhý hráč:" << endl;
    //int m = m_mapa.size();
    //int n = m_mapa.size()-1;
    int m = 9;
    int n = 8;
    hrdinaCekajici = new Hrdina(m, n, "H2");
    umistiHerniObjekt(m, n, hrdinaCekajici);
}

int Mapa::podminkaPohybuHrdinou(Hrdina* hrdina, int x, int y) {
    if ((x <= 9) && (x >= 0) && (y <= 9) && (y >= 0)) {
        if (
                (x >= (hrdina->getPoziceX() - 1)) &&
                (x <= (hrdina->getPoziceX() + 1)) &&
                (y <= (hrdina->getPoziceY() + 1)) &&
                (y >= (hrdina->getPoziceY() - 1))) {
            return 1;
        } else {
            cout << "Muzes se posunout pouze o 1 policko vedle." << endl;
            return 0;
        }
    } else {
        cout << "Muzes se posunout pouze v rozmezich matice 0-9." << endl;
        return 0;
    }
}

void Mapa::posunHrdinu(Hrdina *hrdina, int x, int y) {
    m_mapa.at(hrdina->getPoziceY()).at(hrdina->getPoziceX()) = nullptr;
    umistiHerniObjekt(x, y, hrdina);
    hrdina->setPoziceX(x);
    hrdina->setPoziceY(y);

}


void Mapa::pohniHrdinou(Hrdina *hrdina) {
    cout << "__________________________________________________________________" << endl;
    cout << "Pohyb hrdiny:" << endl;
    int x;
    int y;
    do {
        cout << "Zadej souradnici x." << endl;
        cin >> x;
        cout << "Zadej souradnici y." << endl;
        cin >> y;
    } while (podminkaPohybuHrdinou(hrdina, x, y) == 0);

    if (m_mapa.at(y).at(x) == nullptr) {
        posunHrdinu(hrdina, x, y);
    } else {

        boj(hrdina,m_mapa.at(y).at(x));
    }

   }





void Mapa::boj(Hrdina * kdo, HerniObjekt * skym) {
    if (kdo->getCelkovaSila() >= skym->getCelkovaSila()) {
        int poziceX=skym->getPoziceX();
        int poziceY=skym->getPoziceY();
        delete (m_mapa.at(poziceY).at(poziceX));
        posunHrdinu(kdo, poziceX, poziceY);
        kdo->prictiSuroviny(0,1);
        kdo->prictiSuroviny(1,1);
        kdo->prictiSuroviny(2,1);
        cout << "Vyhral jsi boj." << endl;

    } else if(kdo->getCelkovaSila() < skym->getCelkovaSila()){
        delete (m_mapa.at(kdo->getPoziceY()).at(kdo->getPoziceX()));
        m_mapa.at(kdo->getPoziceY()).at(kdo->getPoziceX()) = nullptr;
        cout << "Prohral jsi boj." << endl;
    }

}

HerniObjekt *Mapa::getHerniObjekt(int x, int y) {
       return m_mapa.at(y).at(x);
}

Hrdina *Mapa::getHrdinaNaTahu() {
    return hrdinaNaTahu;
}

Hrdina *Mapa::getHrdinaCekajici() {
    return hrdinaCekajici;
}

void Mapa::setHrdinaNaTahu(Hrdina *pHrdina) {
    hrdinaNaTahu=pHrdina;

}

void Mapa::setHrdinaCekajici(Hrdina* hrdina) {
    hrdinaCekajici=hrdina;

}













