//
// Created by hladk on 28.04.2019.
//

#include "Hrad.h"

Hrad::Hrad(int poziceX, int poziceY, string kod) : HerniObjekt(poziceX, poziceY, kod) {
    zadejNazev();
    m_uroven = 1;
}


void Hrad::zadejNazev() {
    string nazev;
    cout << "Zadej název hradu: " << endl;
    cin >> nazev;
    if (nazev == "") {
        cout << "Toto neni jmeno" << endl;
        nazev = "Hrad";

    } else {
        m_nazev = nazev;
    }
}


void Hrad::zvysUrovenHradu() {
    m_uroven++;
}


void Hrad::najmiBytosti(int druh, int kolik) {
    m_bytostiVHradu.at(druh)=m_bytostiVHradu.at(druh)+kolik;
}

void Hrad::printInfo() {
    cout << "___________________________________________________" << endl;
    cout << "Hrad: " << m_nazev << endl;
    cout << "Uroven hradu: " << m_uroven << endl;

    cout << endl;
    cout << "Bytosti v hradu: " << endl;
    // cout << m_bytostiVHradu[0]->getPocet();

    for (Bytost *bytost : m_bytostiVHradu) {
        cout << "Bytost: " << bytost->getNazev() << ", pocet techto bytosti: " << bytost->getCelkemBytosti() << endl;
    }
    cout << endl;
    cout << "Bytosti k najmutí:" << endl;

    for (Bytost *bytost : m_bytostiKNajmuti) {
        cout << "Bytost: " << bytost->getNazev() << ", pocet techto bytosti: " << bytost->getCelkemBytosti() << endl;
    }

}


void Hrad::inicializujBKNajmuti() {
    m_bytostiKNajmuti.push_back(Bytost::createBytost(1, 0));
    m_bytostiKNajmuti.push_back(Bytost::createBytost(2, 0));
}

void Hrad::inicializujBVHradu() {
    m_bytostiVHradu.push_back(Bytost::createBytost(1, 0));
    m_bytostiVHradu.push_back(Bytost::createBytost(2, 0));

}

void Hrad::inicializujBytosti() {
    inicializujBKNajmuti();
    inicializujBVHradu();

}

Hrad::~Hrad() {

    for (Bytost *b : m_bytostiKNajmuti) {
        delete (b);
    }
    for (Bytost *b : m_bytostiVHradu) {
        delete (b);
    }
    cout << "Smazan hrad i se vsim uvnitr." << endl;

}

int Hrad::getCelkovaSila() {
    int silaRytiru;
    int silaStrelcu;
    int silaCelkem;
    silaRytiru = m_bytostiVHradu.at(0)->getSila() * m_bytostiVHradu.at(0)->getCelkemBytosti();
    silaStrelcu = m_bytostiVHradu.at(1)->getSila() * m_bytostiVHradu.at(1)->getCelkemBytosti();
    silaCelkem = silaRytiru + silaStrelcu;
    return silaCelkem;
}


int Hrad::getCelkemBytosti() {
    return (m_bytostiVHradu.at(0)->getCelkemBytosti()+m_bytostiVHradu.at(1)->getCelkemBytosti());
}

int Hrad::getUroven() {
    return m_uroven;
}

void Hrad::boj(HerniObjekt* herniObjekt) {
    if (this->getCelkovaSila() < herniObjekt->getCelkovaSila()) {
        delete this;

    } else {
        delete(herniObjekt);
    }
}

void Hrad::bytostiZaTah() {
    m_bytostiKNajmuti.at(0)=m_bytostiKNajmuti.at(0)+(2*m_uroven);
    m_bytostiKNajmuti.at(1)=m_bytostiKNajmuti.at(1)+(1*m_uroven);
}

int Hrad::getPocetBytostiVHradu(int druh) {
    return m_bytostiVHradu.at(druh)->getCelkemBytosti();
}

void Hrad::setPocetBytostiVHradu(int druh, int kolik) {
m_bytostiVHradu.at(druh)->setPocet(kolik);
}


