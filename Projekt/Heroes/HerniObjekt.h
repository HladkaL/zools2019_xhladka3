//
// Created by hladk on 15.04.2019.
//

#ifndef HEROES_HERNIOBJEKT_H
#define HEROES_HERNIOBJEKT_H

#include <iostream>

class HerniObjekt {
protected:
int m_poziceX;
int m_poziceY;
std::string m_kod; //pro odliseni na mape
public:
HerniObjekt(int poziceX, int poziceY, std::string kod);
std::string getKod();
int getPoziceX();
int getPoziceY();
void setPoziceX(int x);
void setPoziceY(int y);
virtual int getCelkovaSila()=0;
virtual int getCelkemBytosti()=0;

};


#endif //HEROES_HERNIOBJEKT_H
