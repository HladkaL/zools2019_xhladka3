//
// Created by hladk on 15.04.2019.
//

#ifndef HEROES_HRDINA_H
#define HEROES_HRDINA_H
#include <iostream>
#include<vector>

#include "HerniObjekt.h"
#include "Bytost.h"
#include "Surovina.h"
#include "Hrad.h"

using namespace std;

class Hrdina : public HerniObjekt {
private:
    vector<Bytost*> m_bytosti;
    vector<Surovina*> m_surovina;
    string m_jmeno;
    string m_rasa;
    int m_zivot;
    Hrad* m_hrad;

    void zadejJmeno(string jmeno);
    void zadejRasu();
    void inicializujBytosti();
    void inicializujSuroviny();


public:
    Hrdina* getHrdinaNaTahu();
    Hrdina* getHrdinaCekajici();
    Hrdina(int poziceX, int poziceY, string kod);
    void inicializujHrdinu();
    string getJmeno();
    string getRasa();
    void printInfo();
    int getCelkovaSila();

    void zabijStrelce(int kolikStrelcu);
    void zabijRytire(int kolikRytiru);

    int getPocetBytosti(int druh);
    void setPocetBytosti(int druh, int pocet);
    int getCelkemBytosti();
    void odectiZivot(int oKolik);
    void prictiSuroviny(int druh, int kolik);
    bool odectiSuroviny(int druh, int kolik);
    void surovinyZaTah();
    void zvysUrovenHradu();
    Hrad* getHrad();
    void kupBytosti(int druh, int kolik);
    void vemBytostiZHradu(int druh, int kolik);

    ~Hrdina();

};


#endif //HEROES_HRDINA_H
