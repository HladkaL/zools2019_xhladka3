//
// Created by hladk on 15.04.2019.
//

#include "Hrdina.h"

Hrdina::Hrdina(int poziceX, int poziceY, string kod) : HerniObjekt(poziceX, poziceY, kod) {
    inicializujHrdinu();
    m_zivot = 100;
    m_hrad = nullptr;
    inicializujSuroviny();
}

void Hrdina::zadejJmeno(string jmeno) {
    if (jmeno == "") {
        cout << "Toto neni jmeno" << endl;
        m_jmeno = "Hrac";

    } else {
        m_jmeno = jmeno;
    }
}


void Hrdina::zadejRasu() {
    string rasa;
    do {
        cin >> rasa;
        if (rasa == "m") {
            m_rasa = "Mág";
        } else if (rasa == "e") {
            m_rasa = "Elf";
        } else {
            cout << "Toto neni rasa! Mág[m], elf[e]." << endl;
            // cin >> rasa;
        }

    } while (rasa != "m" && rasa != "e");

}


void Hrdina::inicializujBytosti() {
    m_bytosti.push_back(Bytost::createBytost(1, 5));
    m_bytosti.push_back(Bytost::createBytost(2, 5));
}


void Hrdina::inicializujHrdinu() {
    cout << "Vytvoření hrdiny:" << endl;
    cout << "Napis jmeno hrdiny: " << endl;
    string jmeno;
    cin >> jmeno;
    zadejJmeno(jmeno);


    cout << "Zvol si rasu. Mág[m], elf[e]." << endl;
    zadejRasu();

    inicializujBytosti();
}


void Hrdina::inicializujSuroviny() {
    //vytvoreni instance zlata a napushovani do vektoru.
    Surovina *zlato = new Surovina("Zlato", 0, 0);
    m_surovina.push_back(zlato);
    //primo vytvoreny novy objekt do vektoru bez promenne.
    m_surovina.push_back(new Surovina("Dřevo", 0, 1));
    m_surovina.push_back(new Surovina("Kamení", 0, 2));

}


string Hrdina::getJmeno() {
    return m_jmeno;
}

string Hrdina::getRasa() {
    return m_rasa;
}

void Hrdina::printInfo() {
    cout << "___________________________________________________" << endl;
    cout << getRasa() << " " << getJmeno() << endl;
    cout << "Hrdina má: " << endl;
    for (Bytost *bytost : m_bytosti) {
        cout << bytost->getNazev() << " pocet: " << bytost->getCelkemBytosti() << endl;
    }


}

Hrdina::~Hrdina() {
    for (Surovina *s : m_surovina) {
        delete s;
    }
}

int Hrdina::getCelkovaSila() {
    int silaRytiru;
    int silaStrelcu;
    int silaCelkem;
    silaRytiru = m_bytosti.at(0)->getSila() * m_bytosti.at(0)->getCelkemBytosti();
    silaStrelcu = m_bytosti.at(1)->getSila() * m_bytosti.at(1)->getCelkemBytosti();
    silaCelkem = silaRytiru + silaStrelcu;
    return silaCelkem;
}

int Hrdina::getPocetBytosti(int druh) {
    return m_bytosti.at(druh)->getCelkemBytosti();
}

void Hrdina::zabijRytire(int kolikRytiru) {
    if (kolikRytiru <= getPocetBytosti(0)) {
        m_bytosti.at(0)->setPocet(getPocetBytosti(0) - kolikRytiru);
    } else {
        m_bytosti.at(0)->setPocet(0);

    }

}

void Hrdina::zabijStrelce(int kolikStrelcu) {
    if (kolikStrelcu <= getPocetBytosti(1)) {
        m_bytosti.at(1)->setPocet(getPocetBytosti(1) - kolikStrelcu);
    } else { m_bytosti.at(1)->setPocet(0); }
}

void Hrdina::setPocetBytosti(int druh, int pocet) {
    m_bytosti.at(druh)->setPocet(pocet);
}

int Hrdina::getCelkemBytosti() {
    return (m_bytosti.at(0)->getCelkemBytosti() + m_bytosti.at(1)->getCelkemBytosti());

}

void Hrdina::odectiZivot(int oKolik) {
    if (oKolik <= m_zivot) {
        m_zivot = m_zivot - oKolik;
    } else
        m_zivot = 0;

}

void Hrdina::prictiSuroviny(int druh, int kolik) {
    m_surovina.at(druh)->prictiPocet(kolik);

}

bool Hrdina::odectiSuroviny(int druh, int kolik) {
    return m_surovina.at(druh)->odectiPocet(kolik);

}

void Hrdina::surovinyZaTah() {
    for(Surovina* surovina : m_surovina){
        surovina->prictiPocet(m_hrad->getUroven()*2);
    }
}

void Hrdina::zvysUrovenHradu() {
    if (odectiSuroviny(2,5)){
        m_hrad->zvysUrovenHradu();
    }

}

Hrad *Hrdina::getHrad() {
    return m_hrad;
}

void Hrdina::kupBytosti(int druh, int kolik) {
    if(odectiSuroviny(1,kolik*2)){
        m_hrad->najmiBytosti(druh,kolik);
}}

void Hrdina::vemBytostiZHradu(int druh, int kolik) {
    if(m_hrad->getPocetBytostiVHradu(druh)<kolik){
        m_bytosti.at(druh)=m_bytosti.at(druh)+kolik;
        m_hrad->setPocetBytostiVHradu(druh,0);
    } else{
        m_hrad->setPocetBytostiVHradu(druh,m_hrad->getPocetBytostiVHradu(druh)-kolik);
        m_bytosti.at(druh)=m_bytosti.at(druh)+kolik;
    }

}





