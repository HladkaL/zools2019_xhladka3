//
// Created by hladk on 15.04.2019.
//

#ifndef HEROES_BYTOST_H
#define HEROES_BYTOST_H

#include <iostream>
#include "HerniObjekt.h"
using namespace std;

class Bytost : public HerniObjekt {
private:
    string m_nazev;
    int m_sila;
    int m_pocet;
    int m_cena;

public:
    Bytost(string nazev, int sila, int pocet, int cena, int poziceX, int poziceY, string kod);


string getNazev();
int getSila();
int getCelkemBytosti();
static Bytost* createBytost(int typ, int kolik);
void setPocet(int kolik);
int getCelkovaSila();
};


#endif //HEROES_BYTOST_H
