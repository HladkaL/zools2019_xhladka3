//
// Created by hladk on 28.04.2019.
//

#ifndef HEROES_HRAD_H
#define HEROES_HRAD_H
#include "HerniObjekt.h"
#include "Bytost.h"
#include <iostream>
#include <vector>

using namespace std;

class Hrad :public HerniObjekt{
private:
    string m_nazev;
    int m_uroven;
    vector<Bytost*> m_bytostiKNajmuti;
    vector<Bytost*> m_bytostiVHradu;


    void inicializujBKNajmuti();
    void inicializujBVHradu();
public:
    Hrad(int poziceX, int poziceY, string kod);
    void zvysUrovenHradu();
    void najmiBytosti(int druh ,int kolik);
    void printInfo();
    void zadejNazev();
    void inicializujBytosti();
    int getCelkovaSila();
    int getCelkemBytosti();
    int getUroven();
    void bytostiZaTah();
    int getPocetBytostiVHradu(int druh);
    ~Hrad();
    void boj(HerniObjekt* herniObjekt);

    void setPocetBytostiVHradu(int druh, int kolik);
};

#endif //HEROES_HRAD_H
